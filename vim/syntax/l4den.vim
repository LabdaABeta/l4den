" Vim syntax file
" Language: L4DEN
" Maintainer: Louis Burke <lburke@labprogramming.net>
" Version: 0.1

if exists("b:current_syntax") || version < 700
    finish
endif

let b:current_syntax = "l4den"

syntax region l4denString12 start=/"{12}/ end=/"{12}/
syntax region l4denString11 start=/"{11}/ end=/"{11}/
syntax region l4denString10 start=/"{10}/ end=/"{10}/
syntax region l4denString9 start=/"{9}/ end=/"{9}/
syntax region l4denString8 start=/"{8}/ end=/"{8}/
syntax region l4denString7 start=/"{7}/ end=/"{7}/
syntax region l4denString6 start=/"{6}/ end=/"{6}/
syntax region l4denString5 start=/"{5}/ end=/"{5}/
syntax region l4denString4 start=/"{4}/ end=/"{4}/
syntax region l4denString3 start=/"{3}/ end=/"{3}/
syntax region l4denString2 start=/"{2}/ end=/"{2}/
syntax region l4denString1 start=/"/ end=/"/

