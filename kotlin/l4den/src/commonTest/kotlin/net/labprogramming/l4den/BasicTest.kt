package net.labprogramming.l4den

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class BasicTest {
    val miniwriter = L4DENWriter()

    @Test
    fun `Basic build test`() {
        assertEquals(
            "test",
            miniwriter.write(L4DEN.create {
                +"test"
            })
        )
    }
}
