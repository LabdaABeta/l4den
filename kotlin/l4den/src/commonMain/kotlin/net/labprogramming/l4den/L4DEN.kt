package net.labprogramming.l4den

// toString() is minified L4DEN notation
sealed class L4DEN {
    sealed class Entry {
        data class Element(val value : L4DEN) : Entry() {
            override fun toString(): String = value.toString()
        }
        data class Entity(val key : Scalar, val value : L4DEN) : Entry() {
            override fun toString(): String = "$key=$value"
        }
    }

    data class Value(
        val value : String,
        val arguments : List<Entry>
    ) {
        fun quotedValue() : String =
            if (value.all { !it.isWhitespace() && it !in "\"'`[]{}()=:" }) value else {
                var delimiter = "\""

                while (delimiter in value) {
                    delimiter = when (delimiter.first()) {
                        '"' -> "'".repeat(delimiter.length)
                        '\'' -> "`".repeat(delimiter.length)
                        '`' -> "\"".repeat(delimiter.length + 1)
                        else -> "\"" // This is a serious error, just try again (likely randomly corrupted data)
                    }
                }

                "$delimiter$value$delimiter"
            }

        fun argString() : String = if (arguments.isNotEmpty()) "(" + arguments.joinToString("") + ")" else ""

        override fun toString(): String = quotedValue() + argString()
    }

    data class Scalar(
        val value : Value,
        val type : Value?
    ) : L4DEN() {
        override fun toString(): String = value.toString() + if (type != null) ":$type" else ""
    }

    data class Object(
        val ordered : Boolean,
        val contents : List<Entry>
    ) : L4DEN() {
        override fun toString(): String = (if (ordered) "[" else "{") + contents.joinToString("") + (if (ordered) "]" else "}")
    }

    companion object {
        fun create(build : L4DENBuilder.() -> Unit) : List<Entry> {
            val builder = L4DENBuilder()

            builder.build()

            return builder.entries
        }
    }
}

typealias L4DENDocument = List<L4DEN.Entry>
