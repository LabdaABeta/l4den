package net.labprogramming.l4den

class L4DENBuilder {
    val entries : MutableList<L4DEN.Entry> = mutableListOf()

    operator fun String.unaryPlus() {
        entries.add(L4DEN.Entry.Element(L4DEN.Scalar(L4DEN.Value(this, emptyList()), null)))
    }

    operator fun L4DEN.unaryPlus() {
        entries.add(L4DEN.Entry.Element(this))
    }

    fun set(contents : L4DENBuilder.() -> Unit) : L4DEN.Object {
        val builder = L4DENBuilder()

        builder.contents()

        return L4DEN.Object(
            ordered = false,
            contents = builder.entries
        )
    }

    fun list(contents : L4DENBuilder.() -> Unit) : L4DEN.Object {
        val builder = L4DENBuilder()

        builder.contents()

        return L4DEN.Object(
            ordered = true,
            contents = builder.entries
        )
    }

    fun String.invoke(args : L4DENBuilder.() -> Unit) : L4DEN.Value {
        val builder = L4DENBuilder()

        builder.args()

        return L4DEN.Value(
            value = this,
            arguments = builder.entries
        )
    }

    infix fun String.type(t : L4DEN.Value) : L4DEN.Scalar = L4DEN.Scalar(L4DEN.Value(this, emptyList()), t)
    infix fun L4DEN.Value.type(t : L4DEN.Value) : L4DEN.Scalar = L4DEN.Scalar(this, t)

    infix fun L4DEN.Scalar.gets(value : L4DEN) {
        entries.add(L4DEN.Entry.Entity(this, value))
    }
    infix fun String.gets(value : L4DEN) {
        entries.add(L4DEN.Entry.Entity(L4DEN.Scalar(L4DEN.Value(this, emptyList()), null), value))
    }
    infix fun L4DEN.Scalar.gets(value : String) {
        entries.add(L4DEN.Entry.Entity(this, L4DEN.Scalar(L4DEN.Value(value, emptyList()), null)))
    }
    infix fun String.gets(value : String) {
        entries.add(L4DEN.Entry.Entity(L4DEN.Scalar(L4DEN.Value(this, emptyList()), null), L4DEN.Scalar(L4DEN.Value(value, emptyList()), null)))
    }
}
