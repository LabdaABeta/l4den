package net.labprogramming.l4den

// Defaults are canonical
class L4DENWriter(
    val indent : Int? = null, // size of indent, in spaces if positive or tabs if negative, null means no newlines (minify usually)
    val alignTypes : Boolean = false, // whether to align :s
    val alignValues : Boolean = false, // whether to align =s
    val lineLength : Int = 80, // at what line length to split arguments/lists/sets across multiple lines
    val preTypePadding : Int = 0, // how much space to prefix : with
    val postTypePadding : Int = 1, // how much space to suffix : with
    val preValuePadding : Int = 1, // how much space to prefix = with
    val postValuePadding : Int = 1, // how much space to suffix = with
    val delimiterPreference : String = "\"'`", // which delimiters to prefer over which
) {
    private fun helper(document : L4DENDocument, depth : Int) : String {
        TODO()
    }

    fun write(document : L4DENDocument) : String =
        if (indent == null)
            document.joinToString("")
        else
            helper(document, 0)
}