package net.labprogramming.l4den

data class StreamLocation(val row : Int, val col : Int)
data class L4DENToken(val type : L4DENTokenType, val contents : String, val location: StreamLocation)

class L4DENTokenizationError(val location: StreamLocation) : Exception() {
    override val message: String = "Unclosed string at end of file ($location)"
}

fun Sequence<Char>.toL4DENStream() : Sequence<L4DENToken> {
    var buffer = ""
    var row = 1
    var col = 0

    // Returns true if buffer is a "closed" string (either unquoted or quote is done)
    fun isBufferClosed() : Boolean =
        when {
            buffer.isEmpty() -> false
            buffer.first() in "\"`'" -> {
                val delimiter = buffer.takeWhile { it == buffer.first()}

                buffer.length > delimiter.length && buffer.endsWith(delimiter)
            }
            else -> true
        }

    fun bufferValue() : String =
        if (buffer.first() in "\"`'") {
            val delimiter = buffer.takeWhile { it == buffer.first() }

            buffer.removePrefix(delimiter).removeSuffix(delimiter)
        } else {
            buffer.trim().replace("\\w+".toRegex(), " ")
        }

    return flatMap {
        if (it == '\n') {
            row++
            col = 0
        } else
            col++

        val loc = StreamLocation(row, col)

        if (buffer.isEmpty()) {
            when (it) {
                '(' -> listOf(L4DENToken(L4DENTokenType.OPENP, "(", loc))
                ')' -> listOf(L4DENToken(L4DENTokenType.CLOSEP, ")", loc))
                ',' -> listOf(L4DENToken(L4DENTokenType.COMMA, ",", loc))
                ':' -> listOf(L4DENToken(L4DENTokenType.COLON, ":", loc))
                '=' -> listOf(L4DENToken(L4DENTokenType.EQUALS, "=", loc))
                '[' -> listOf(L4DENToken(L4DENTokenType.OPENB, "[", loc))
                ']' -> listOf(L4DENToken(L4DENTokenType.CLOSEB, "]", loc))
                '{' -> listOf(L4DENToken(L4DENTokenType.BEGIN, "{", loc))
                '}' -> listOf(L4DENToken(L4DENTokenType.END, "}", loc))

                else -> {
                    buffer += it
                    listOf()
                }
            }
        } else {
            if (buffer.first() in "\"'`") {
                buffer += it

                if (isBufferClosed()) {
                    val delimiter = buffer.takeWhile { x -> x == buffer.first()}

                    listOf(L4DENToken(L4DENTokenType.LITERAL, buffer.removePrefix(delimiter).removeSuffix(delimiter), loc)).also { buffer = "" }
                } else {
                    listOf()
                }
            } else {
                when (it) {
                    '(' -> listOf(L4DENToken(L4DENTokenType.LITERAL, buffer, loc), L4DENToken(L4DENTokenType.OPENP, "(", loc)).also { buffer = "" }
                    ')' -> listOf(L4DENToken(L4DENTokenType.LITERAL, buffer, loc), L4DENToken(L4DENTokenType.CLOSEP, ")", loc)).also { buffer = "" }
                    ',' -> listOf(L4DENToken(L4DENTokenType.LITERAL, buffer, loc), L4DENToken(L4DENTokenType.COMMA, ",", loc)).also { buffer = "" }
                    ':' -> listOf(L4DENToken(L4DENTokenType.LITERAL, buffer, loc), L4DENToken(L4DENTokenType.COLON, ":", loc)).also { buffer = "" }
                    '=' -> listOf(L4DENToken(L4DENTokenType.LITERAL, buffer, loc), L4DENToken(L4DENTokenType.EQUALS, "=", loc)).also { buffer = "" }
                    '[' -> listOf(L4DENToken(L4DENTokenType.LITERAL, buffer, loc), L4DENToken(L4DENTokenType.OPENB, "[", loc)).also { buffer = "" }
                    ']' -> listOf(L4DENToken(L4DENTokenType.LITERAL, buffer, loc), L4DENToken(L4DENTokenType.CLOSEB, "]", loc)).also { buffer = "" }
                    '{' -> listOf(L4DENToken(L4DENTokenType.LITERAL, buffer, loc), L4DENToken(L4DENTokenType.BEGIN, "{", loc)).also { buffer = "" }
                    '}' -> listOf(L4DENToken(L4DENTokenType.LITERAL, buffer, loc), L4DENToken(L4DENTokenType.END, "}", loc)).also { buffer = "" }

                    else -> {
                        buffer += it
                        listOf()
                    }
                }
            }
        }
    } + if (buffer.isNotEmpty()) {
        val loc = StreamLocation(row, col)

        if (isBufferClosed()) {
            if (buffer.first() in "\"'`") {
                val delimiter = buffer.takeWhile { it == buffer.first() }

                listOf(L4DENToken(L4DENTokenType.LITERAL, buffer.removePrefix(delimiter).removeSuffix(delimiter), loc))
            } else {
                listOf(L4DENToken(L4DENTokenType.LITERAL, buffer, loc))
            }
        } else {
            throw L4DENTokenizationError(loc)
        }
    } else { emptyList() } + listOf(L4DENToken(L4DENTokenType.EOF, "", StreamLocation(row, col)))
}
