package net.labprogramming.l4den

enum class L4DENTokenType(val repr : Char) {
    LITERAL('"'),
    EOF('$'),
    OPENP('('),
    CLOSEP(')'),
    COMMA(','),
    COLON(':'),
    EQUALS('='),
    OPENB('['),
    CLOSEB(']'),
    BEGIN('{'),
    END('}'),
}