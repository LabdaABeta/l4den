package net.labprogramming.l4den

class L4DENParsingException(val location: StreamLocation, val expected : String, val actual : L4DENTokenType) : Exception() {
    override val message = "Parsing error at $location. Expected $expected, got $actual"
}

class L4DENParser(val input : Sequence<Char>) {
    private val tokens = input.toL4DENStream()
    private val iter = tokens.iterator()
    private var symbol = iter.next() // guaranteed to have at least one element since it is EOF terminated

    private tailrec fun body(appendTo : List<L4DEN.Entry> = emptyList()) : List<L4DEN.Entry> =
        when (symbol.type) {
            L4DENTokenType.EOF, L4DENTokenType.CLOSEP, L4DENTokenType.CLOSEB, L4DENTokenType.END -> appendTo
            L4DENTokenType.LITERAL, L4DENTokenType.OPENB, L4DENTokenType.BEGIN -> body(listOf(entity()))
            else -> throw L4DENParsingException(symbol.location, "a list of entries", symbol.type)
        }

    private fun entity() : L4DEN.Entry =
        when (symbol.type) {
            L4DENTokenType.LITERAL, L4DENTokenType.OPENB, L4DENTokenType.BEGIN -> {
                val v = value()
                val o = optAssignment()

                if (o != null) {
                    if (v is L4DEN.Scalar)
                        L4DEN.Entry.Entity(v, o)
                    else
                        throw L4DENParsingException(symbol.location, "a key must be a scalar", symbol.type)
                } else
                    L4DEN.Entry.Element(v)
            }
            else -> throw L4DENParsingException(symbol.location, "an entry", symbol.type)
        }

    private fun optAssignment() : L4DEN? =
        when (symbol.type) {
            L4DENTokenType.EOF, L4DENTokenType.LITERAL, L4DENTokenType.CLOSEP, L4DENTokenType.OPENB, L4DENTokenType.CLOSEB, L4DENTokenType.BEGIN, L4DENTokenType.END -> null
            L4DENTokenType.EQUALS -> {
                symbol = iter.next()

                value()
            }

            else -> throw L4DENParsingException(symbol.location, "an optional assignment", symbol.type)
        }

    private fun value() : L4DEN =
        when (symbol.type) {
            L4DENTokenType.LITERAL -> scalar()
            L4DENTokenType.OPENB, L4DENTokenType.OPENP -> l4object()
            else -> throw L4DENParsingException(symbol.location, "a value", symbol.type)
        }

    private fun scalar() : L4DEN.Scalar {
        if (symbol.type != L4DENTokenType.LITERAL)
            throw L4DENParsingException(symbol.location, "a literal", symbol.type)

        val literal = symbol
        symbol = iter.next()

        val args = arguments()
        val t = type()

        return L4DEN.Scalar(L4DEN.Value(literal.contents, args), t)
    }

    private fun arguments() : List<L4DEN.Entry> =
        when (symbol.type) {
            L4DENTokenType.OPENP -> {
                symbol = iter.next()

                val result = body()

                if (symbol.type != L4DENTokenType.CLOSEP)
                    throw L4DENParsingException(symbol.location, "a close paren", symbol.type)

                result
            }
            else -> emptyList()
        }

    private fun type() : L4DEN.Value? =
        when (symbol.type) {
            L4DENTokenType.COLON -> {
                symbol = iter.next()

                if (symbol.type != L4DENTokenType.LITERAL)
                    throw L4DENParsingException(symbol.location, "a literal value", symbol.type)

                val literal = symbol
                symbol = iter.next()

                val args = arguments()

                L4DEN.Value(literal.contents, args)
            }

            L4DENTokenType.OPENP ->
                throw L4DENParsingException(symbol.location, "an optional type", symbol.type)

            else -> null
        }

    private fun l4object() : L4DEN.Object =
        when (symbol.type) {
            L4DENTokenType.OPENB -> {
                symbol = iter.next()

                val contents = body()

                if (symbol.type != L4DENTokenType.CLOSEB)
                    throw L4DENParsingException(symbol.location, "a close bracket", symbol.type)

                L4DEN.Object(true, contents)
            }

            L4DENTokenType.BEGIN -> {
                symbol = iter.next()

                val contents = body()

                if (symbol.type != L4DENTokenType.END)
                    throw L4DENParsingException(symbol.location, "a close brace", symbol.type)

                L4DEN.Object(false, contents)
            }

            else ->
                throw L4DENParsingException(symbol.location, "an object", symbol.type)
        }

    fun parse() : L4DENDocument {
        val result = body()
        if (symbol.type != L4DENTokenType.EOF)
            throw L4DENParsingException(symbol.location, "end of input", symbol.type)

        return result
    }
}

