#include "l4den.h"

#include <stdio.h>

char *read_entire_file(char *fname, size_t *len) {
    char *buffer;
    FILE *input = fopen(fname, "rb");

    if (!input)
        return 0;

    fseek(input, 0, SEEK_END);
    *len = ftell(input);
    fseek(input, 0, SEEK_SET);

    buffer = malloc(*len + 1);

    if (!buffer) {
        fclose(input);
        return 0;
    }

    fread(buffer, 1, *len, input);
    fclose(f);

    return buffer;
}

int main(int argc, char *argv[]) {
    char *dump;
    struct l4den_entry *parsed;
    size_t len;
    struct l4den_error error;
    char *data = read_entire_file(argv[1], &len);

    if (!data)
        return 1;

    parsed = l4den_decode(data, len, &error);
    free(data);

    if (!parsed) {
        return printf("ERROR: %c at %d, %d expected %c got %c\n",
            error.code, error.location.row, error.location.col,
            error.expected, error.actual);
    }

    len = l4den_encode_size(parsed);

    if (!len) {
        return 2;
    }

    dump = malloc(len + 1);

    len = l4den_encode(dump, len + 1, parsed, &error);

    if (!len) {
        return printf("ERROR: %c at %d, %d expected %c got %c\n",
            error.code, error.location.row, error.location.col,
            error.expected, error.actual);
    }

    l4den_free_entries(parsed);
    free(dump);
    return 0;
}
