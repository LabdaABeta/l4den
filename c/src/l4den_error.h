/*!
 * \file l4den_error.h
 * \author Louis Burke
 * \brief This header file defines the types for errors reported by the API.
 *
 * These errors come with a default printer, or can be parsed by the user.
 */
#ifndef L4DEN_ERROR_H
#define L4DEN_ERROR_H "0.0.1" /*!< Library version and single inclusion token */

enum l4den_error_code {
    L4DEN_ERROR_NULL_BUFFER, /*!< You passed a null pointer for a buffer */
    L4DEN_ERROR_OUT_OF_MEMORY, /*!< Ran out of memory during operation */
    L4DEN_PARSE_ERROR, /*!< Error in parsing */
    L4DEN_CONSTRUCT_ERROR, /*!< Error in output construction */
};

struct l4den_error {
    enum l4den_error_code code;

    struct {
        int row;
        int col;
    } location;

    char expected;
    char actual;
};

// TODO: Document (not threadsafe)
char *l4den_error_to_string(const struct l4den_error *error);

#endif /* L4DEN_ERROR_H */
