/*!
 * \file l4den_types.h
 * \author Louis Burke
 * \brief This header file defines the types for storing and manipulating L4DEN.
 *
 * These represent L4DEN objects as null-value terminated entries. This common
 * representation is an array of l4den_entry structures terminated by an entry
 * with the value field set to NULL.
 *
 * The reason that this API uses an array while the static API uses a linked
 * list is primarily cache locality. Most likely any client will wish to iterate
 * over the entries in a L4DEN implementation and perhaps revisit interesting
 * ones. Having all the top-level pointers in a continuous block of memory makes
 * the iteration much less likely to cause cache misses.
 */

#ifndef L4DEN_TYPES_H
#define L4DEN_TYPES_H "0.0.1" /*!< Library version and single inclusion token */

#include <stddef.h>

struct l4den;

/*!
 * \brief This structure represents an entity or an element.
 *
 * This contains two pointers to l4den.
 * A null key represents an element.
 * A null value represents the end of a list of entries (much like `\0` in
 * c-strings).
 */
struct l4den_entry {
    struct l4den *key; /*!< null if not an assignment */
    struct l4den *value; /*!< null to mark end of list  */
};

/*!
 * \brief This structure represents a literal value with optional arguments.
 *
 * This is used both for scalar values and their types.
 * Note that the value string may not be null-terminated. It may also contain
 * null values within it. Use the length field!
 */
struct l4den_value {
    char *value; /*!< The literal value of the value. Not necessarily null terminted. */
    size_t length; /*!< The length of the value. */
    struct l4den_entry *arguments; /*!< The arguments of the value, or null. */
};

/*!
 * \brief This structure represents a value (either an object or a scalar)
 *
 * This serves as a tagged union of a scalar or an object. The is_value field is
 * used as the determinant.
 */
struct l4den {
    int is_value; /*!< If non-zero, this is a scalar, otherwise an object. */

    union {
        struct {
            struct l4den_value *value; /*!< The scalar value. */
            struct l4den_value *type; /*!< The scalar value type. nullable. */
        } value; /*!< Only valid if is_value is non-zero */

        struct {
            int ordered; /*!< If non-zero, then the body is ordered. */
            struct l4den_entry *body; /*!< A null-value terminated list. */
        } object; /*!< Only valid if is_value is zero */
    };
};

/*!
 * \brief Frees all memory associated with an array of l4den_entry objects.
 *
 * \param[in] buf  The null-value terminated array of entries to free.
 * \param[in] deep If non-zero, free l4den_value value fields.
 *
 * Note that l4den_value value fields should not be freed when they are parsed
 * from an array of chars as they will be raw pointers to the internals of said
 * array.
 */
void l4den_free_entries(struct l4den_entry *buf, int deep);

#endif /* L4DEN_TYPES_H */
