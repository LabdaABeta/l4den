#ifndef SCANNER_H
#define SCANNER_H

#define TERMINAL_LITERAL '"'
#define TERMINAL_EOF     '$'
#define TERMINAL_OPENP   '('
#define TERMINAL_CLOSEP  ')'
#define TERMINAL_COLON   ':'
#define TERMINAL_EQUALS  '='
#define TERMINAL_OPENB   '['
#define TERMINAL_CLOSEB  ']'
#define TERMINAL_BEGIN   '{'
#define TERMINAL_END     '}'

#define NONTERMINAL_L4DEN          'L'
#define NONTERMINAL_BODY           'B'
#define NONTERMINAL_ENTITY         'E'
#define NONTERMINAL_OPT_ASSIGNMENT '?'
#define NONTERMINAL_VALUE          'V'
#define NONTERMINAL_SCALAR         'S'
#define NONTERMINAL_ARGS           'A'
#define NONTERMINAL_TYPE           'T'
#define NONTERMINAL_OBJECT         'O'

/* Action rules to instruct LL parser to construct nodes */
#define RULE_0 '0' /* L4DEN -> BODY $end */
#define RULE_1 '1' /* BODY -> */
#define RULE_2 '2' /* BODY -> ENTITY BODY */
#define RULE_3 '3' /* ENTITY -> VALUE OPT_ASSIGNMENT */
#define RULE_4 '4' /* OPT_ASSIGNMENT -> */
#define RULE_5 '5' /* OPT_ASSIGNMENT -> = VALUE */
#define RULE_6 '6' /* VALUE -> SCALAR */
#define RULE_7 '7' /* VALUE -> OBJECT */
#define RULE_8 '8' /* SCALAR -> literal ARGS TYPE */
#define RULE_9 '9' /* ARGS -> */
#define RULE_10 'a' /* ARGS -> ( BODY ) */
#define RULE_11 'b' /* TYPE -> */
#define RULE_12 'c' /* TYPE -> : literal ARGS */
#define RULE_13 'd' /* OBJECT -> [ BODY ] */
#define RULE_14 'e' /* OBJECT -> { BODY } */

#define SCANNER_ERROR '!'

/*
 * Invarint: input[0] is at row:col of total input and input[len] is last
 * element of input.
 */
struct l4den_scanner {
    const char *input;
    size_t len;
    int row;
    int col;
};

void l4den_scanner_load(struct l4den_scanner *scanner, const char *input, size_t inlen);
char l4den_scanner_next(struct l4den_scanner *scanner, char **start, size_t *len);

#endif /* SCANNER_H */
