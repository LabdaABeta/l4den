#ifndef UTIL_H
#define UTIL_H

/* Allocates memory for X, running the attached block if allocation fails. */
#define ALLOCATE_OR(X) if (!((X) = malloc(sizeof *(X))))

#endif /* UTIL_H */
