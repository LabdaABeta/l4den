#ifndef ELEMENT
    #error "ELEMENT not defined, should be the type of elements in the stack."
#endif

#ifndef PREFIX
    #error "PREFIX not defined, should be the prefix to add to functions and types."
#endif

#define PP_CONCAT_IMPL(X, Y) X ## _ ## Y
#define PP_CONCAT(X, Y) PP_CONCAT_IMPL(X, Y)
#define P(X) PP_CONCAT(PREFIX, X)

#define INITIAL_CAPACITY 0x100

struct PREFIX {
    ELEMENT *contents;
    size_t size;
    size_t top;
};

static int P(init)(struct PREFIX *stack);
static void P(free)(struct PREFIX *stack);
static size_t P(size)(const struct PREFIX *stack);
static int P(push)(struct PREFIX *stack, ELEMENT value);
static ELEMENT P(peek)(const struct PREFIX *stack);
static void P(pop)(struct PREFIX *stack);

static int P(init)(struct PREFIX *stack) {
    stack->contents = malloc((sizeof *stack->contents) * INITIAL_CAPACITY);
    stack->size = INITIAL_CAPACITY;
    stack->top = 0;

    return stack->contents != 0;
}

static void P(free)(struct PREFIX *stack) {
    free(stack->contents);
}

static size_t P(size)(const struct PREFIX *stack) {
    return stack->top;
}

static int P(push)(struct PREFIX *stack, ELEMENT value) {
    if (stack->top > stack->size) {
        if (!realloc(stack->contents, stack->size << 1))
            return 1;
        stack->size <<= 1;
    }

    stack->contents[stack->top++] = value;
    return 0;
}

static ELEMENT P(peek)(const struct PREFIX *stack) {
    return stack->contents[stack->top - 1];
}

static void P(pop)(struct PREFIX *stack) {
    --stack->top;
}

#undef P
#undef PP_CONCAT
#undef PP_CONCAT_IMPL
#undef PREFIX
#undef ELEMENT
