/* vi: ft=c
 * Provides a generic L4DEN parser implementation.
 */

#ifndef PREFIX
    #error "PREFIX not defined, it should be an identifier to prepend to the recursive descent parser function names."
#endif

#ifndef STATE_ARGS
    #error "STATE_ARGS not defined, it should be the raw arguments added to each recursive descent function in order to successfully pass the input stream."
#endif

#ifndef STATE_ARG
    #error "STATE_ARG not defined, it should be the names of the arguments added to each recursive descent function in order to pass the input stream."
#endif

#ifndef NEXT_CHAR
    // TODO: May need ability to "peek"? Perhaps NEXT_CHAR and CURRENT_CHAR?
    #error "NEXT_CHAR not defined, it should be a function-like macro that when called will resolve to an R-value of type char representing the next character read from the input stream."
#endif

// TODO: More generics, e.g. how to output

#define PP_CONCAT_IMPL(X, Y) X ## Y
#define PP_CONCAT(X, Y) PP_CONCAT_IMPL(X, Y)
#define P(X) PP_CONCAT(PREFIX, X)



static int P(_accept)(enum token_type token, STATE_ARGS) {
    // TODO
}

// TODO: undef everything


