/*!
 * \file l4den_s.h
 * \author Louis Burke
 * \brief This header file defines an embedded API for reading and writing
 * L4DEN.
 *
 * As opposed to the standard API as defined in l4den.h, this API does not
 * require any dynamic memory management, nor the inclusion of any standard
 * libraries. As such it also does not reference any other l4den API. It is
 * a self-contained standalone API.
 *
 * This makes this implementation easier to use in certain environments, but
 * also adds certain constraints to it.
 *
 * In particular, error handling is far less robust as this implementation is
 * intended to be as small as possible.
 *
 * Additionally recursive descent is used to parse the input so as to not rely
 * on a dynamic stack. Thus the maximum size of the parse stack may be
 * significantly smaller.
 */
#ifndef L4DEN_S_H
#define L4DEN_S_H "0.0.1" /*!< Library version and single inclusion token */

/*!
 * \name Static memory management objects.
 * \brief Objects to represent L4DEN without requiring dynamic memory.
 *
 * These may be used when performance is critical and maximum structure sizes
 * are known in advance or when dynamic memory management is not an option.
 *
 * However, as this requires contiguous blocks of memory for interlinking it has
 * a smaller maximum size. It can be a bit more difficult to work with, and only
 * supports parsing from a c-string as references to locations within the input
 * string are used to store string values.
 *
 * Functions which use these values are prefixed with l4den_s_ instead of the
 * typical l4den_ to indicate this.
 */
/*!@{*/
struct l4den_s;
/*!
 * \brief This structure represents a literal value with optional arguments.
 *
 * This is used both for scalar values and their types.
 * The referenced value is a pointer into the input string.
 */
struct l4den_s_value {
    const char *value; /*!< The literal value of the value. Not null-terminated. If this is null then the value itself is a null value. */
    int length; /*!< The length of the value. */
    struct l4den_s *arguments; /*!< The arguments of the value, or null. */
};

/*!
 * \brief This structure represents a value (either an object or a scalar)
 *
 * This serves as a tagged union of a scalar or an object. The is_value field is
 * used as the determinant.
 *
 * This also serves as a node in a linked list.
 *
 * \invariant
 * If key is not null, then key->key must be null (as the key of this value
 * cannot itself have a key).
 */
struct l4den_s {
    int is_value; /*!< If non-zero, this is a value, otherwise a scalar. */

    union {
        struct {
            struct l4den_s_value value; /*!< The scalar value. */
            struct l4den_s_value type; /*!< The scalar value type. value-nullable. */
        } value; /*!< Only valid if is_value is non-zero */

        struct {
            int ordered; /*!< If non-zero, then the body is ordered. */
            struct l4den_s *body; /*!< The contents of the object. */
        } object; /*!< Only valid if is_value is zero */
    };

    struct l4den_s *key; /*< The key associated to this value, or null if this value is an element or key. */
    struct l4den_s *next; /*< The next value in the structure, or null if this is the last value in the body. */
};
/*!@}*/

/*!
 * \brief Decodes a static entry list from a c-string input.
 *
 * \param[in,out] buf     The buffer whose elements are linked and filled to store the result.
 * \param[in]     bufsize The length of the above buffer, in entries.
 * \param[in]     input   The string containing L4DEN to parse.
 * \param[in]     inlen   The length of the above string.
 *
 * \return The root element representing the first entry in the parsed L4DEN, or null on error.
 */
struct l4den_s l4den_s_decode(struct l4den_s *buf, int bufsize, const char *input, int inlen);

/*!
 * \brief Encodes a static entry tree into a c-string.
 *
 * \param[out] buf    The buffer into which a null-terminated string will be written.
 * \param[in]  buflen The length of the above buffer, in characters.
 * \param[in]  data   The data to encode.
 *
 * \return The number of characters written, or -1 on error.
 */
int l4den_s_encode(char *buf, int buflen, const struct l4den_s_entry *data);

/*!
 * \brief Calculates the total count of l4den_s objects required to represent
 * the data in the input L4DEN string.
 *
 * This differs from l4den_decode_size in that it includes the count for all
 * nested structures as well as top-level ones.
 *
 * \param[in] input The string containing L4DEN to parse.
 *
 * \return The total number of entries represented by the input c-string plus one, or zero on error.
 */
unsigned int l4den_s_decode_size(const char *input);

#endif /* L4DEN_S_H */
