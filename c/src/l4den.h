/*!
 * \file l4den.h
 * \author Louis Burke
 * \brief This header file defines an API for reading and writing L4DEN.
 *
 * L4DEN stands for the L4 Data Exchange Notation. It is to L4 what JSON is to
 * Javascript. It can represent strongly typed values, ordered and unordered
 * lists of heterogeneous data, and mixed key/value assignments and standalone
 * values.
 *
 * A L4DEN document consists of a series of entities and/or elements.
 *
 * L4DEN entities are just key/value pairs separated by an equals sign (=).
 * Elements are single standalone values without assignment. Unlike many
 * serialization formats which require all data to be either entirely key/value
 * pairs or entirely standalone values, L4DEN allows the two to mix freely.
 *
 * L4DEN values can be either objects or scalars.
 *
 * L4DEN objects, like documents, consist of a series of entities and/or
 * elements.  They are wrapped in either braces or brackets ({} or []) to
 * indicate whether the order of the contained elements is considered to be
 * relevant ([]) or not ({}). Colloquially these are often called lists ([]
 * \-- ordered) and sets ({} \-- unordered). However it is important to remember
 * that both may contain key/value pairs as well as standalone values.
 *
 * L4DEN scalars consist of a literal value with optional arguments and optional
 * type.
 *
 * Arguments are placed after the value within parentheses (()). They, like
 * objects and documents, consist of a series of entities and/or elements.
 *
 * Types are placed after the value and any arguments, separated by a colon (:).
 * A type is also a literal value with optional arguments, but a type cannot
 * itself have a type.
 *
 * A valid L4DEN document conforms to the following grammar (in bison syntax):
 *
 * \code{.y}
 * L4DEN : BODY ;
 * BODY : | ENTITY BODY | ELEMENT BODY;
 * ENTITY : KEY '=' VALUE ;
 * ELEMENT : VALUE ;
 * KEY : SCALAR ;
 * VALUE : SCALAR | OBJECT ;
 * SCALAR : literal ARGS TYPE ;
 * ARGS : | '(' BODY ')' ;
 * TYPE : | ':' literal ARGS ;
 * OBJECT : LIST | SET ;
 * LIST : '[' BODY ']' ;
 * SET : '{' BODY '}' ;
 * \endcode
 *
 * This can be made LL(1) as follows:
 *
 * \code{.y}
 * L4DEN : BODY ;
 * BODY : | ENTITY BODY ;
 * ENTITY : KEY OPT_ASSIGNMENT ;
 * OPT_ASSIGNMENT : | '=' VALUE ;
 * KEY : VALUE ;
 * VALUE : SCALAR | OBJECT ;
 * SCALAR : literal ARGS TYPE ;
 * ARGS : | '(' BODY ')' ;
 * TYPE : | ':' literal ARGS ;
 * OBJECT : LIST | SET ;
 * LIST : '[' BODY ']' ;
 * SET : '{' BODY '}' ;
 * \endcode
 *
 * A literal value above indicates a single literal string. A literal string is
 * either any series of non-whitespace characters with no quotes ('`") or other
 * active characters ([](){}:=) in it, or a string delimited by some number of
 * repeated quote marks. As there are three possible quote marks to use and only
 * two boundaries to strings, any string can be represented. For example the
 * string:
 *
 * \code{.unparsed}
 * "Go to `insert expletive here` sleep!", the husbands'
 * \endcode
 *
 * Could be encoded as:
 *
 * \code{.unparsed}
 * ``"Go to `insert expletive here` sleep!", the husbands'``
 * \endcode
 *
 * Note that canonically the shortest possible delimiter is used with preference
 * for " over ' and ' over `.
 *
 * Whitespace outside of quoted literals is stripped.
 *
 * While comments are not directly supported, they may be used if the data
 * stream is first stripped of them, or they may be implemented as string
 * elements to be ignored by consumers.
 */
#ifndef L4DEN_H
#define L4DEN_H "0.0.1" /*!< Library version and single inclusion token  */

#include "l4den_types.h"
#include "l4den_error.h"

/*!
 * \brief Decodes a sequence of entries from a c-string input.
 *
 * \param[in]  input The string containing L4DEN to parse.
 * \param[in]  inlen The length of the above string.
 * \param[out] error The structure in which to store an error, or null.
 *
 * \return A null-value terminated list of entries, or NULL on error.
 */
struct l4den_entry *l4den_decode(const char *input, size_t inlen, struct l4den_error *error);

/*!
 * \brief Encodes a sequence of entries into a c-string.
 *
 * \param[out] buf    The buffer into which a null-terminated string will be written.
 * \param[in]  buflen The length of the above buffer, in characters.
 * \param[in]  data   The data to encode.
 * \param[out] error  The structure in which to store an error, or null.
 *
 * \return The number of characters written plus one, or zero on error.
 */
size_t l4den_encode(char *buf, size_t buflen, const struct l4den_entry *data, struct l4den_error *error);

/*!
 * \brief Calculates the number of bytes required to encode the input data in L4DEN format.
 *
 * \param[in] data The data whose encoded size to check.
 *
 * \return The number of bytes required to encode the input data in L4DEN format plus one, or zero on error.
 */
size_t l4den_encode_size(const struct l4den_entry *data);

#endif /* L4DEN_H */
