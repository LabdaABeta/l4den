/*
 * Actually parses the simplified:
 *
 * BODY : | ENTITY BODY ;
 * ENTITY : VALUE OPT_ASSIGNMENT ;
 * OPT_ASSIGNMENT : | '=' VALUE ;
 * VALUE : SCALAR | OBJECT ;
 * SCALAR : literal ARGS TYPE ;
 * ARGS : | '(' BODY ')' ;
 * TYPE : | ':' literal ARGS ;
 * OBJECT : '[' BODY ']' | '{' BODY '}';
 */
#include "l4den.h"

#include "util.h"

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#define ELEMENT char
#define PREFIX symbol_stack
#include "dynamic_stack.inc"

#define ELEMENT struct l4den_entry
#define PREFIX entry_stack
#include "dynamic_stack.inc"

struct semantic_item {
    char symbol;

    union {
        struct {
            char *start;
            size_t len;
        } terminal;

        struct {
            struct l4den_entry *entries;
        } accept, args;

        struct {
            struct entry_stack entries;
        } body;

        struct {
            struct l4den_entry entry;
        } entity;

        struct {
            struct l4den *value; /* nullable */
        } opt_assignment;

        struct {
            struct l4den value;
        } value, scalar, object;

        struct {
            struct l4den_value *type;
        } type;
    };
};
#define ELEMENT struct semantic_item
#define PREFIX semantic_stack
#include "dynamic_stack.inc"

#include "scanner.h"

struct parser_state {
    struct symbol_stack symbols;
    struct semantic_stack semantics;
    struct l4den_scanner scanner;
    struct l4den_error *error;
};

static void parser_state_report_error(struct parser_state *state, enum l4den_error_code code, char expected, char actual) {
    if (state->error) {
        state->error->code = code;
        state->error->expected = expected;
        state->error->actual = actual;
        state->error->location.row = state->scanner.row;
        state->error->location.col = state->scanner.col;
    }
}

static void parser_state_free(struct parser_state *state) {
    /* TODO: Free each element of semantic_stack before freeing both stacks */
}

static int parser_state_push_symbol(struct parser_state *state, char symbol) {
    if (!symbol_stack_push(&state->symbols, symbol)) {
        parser_state_free(state);
        parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
        return 0;
    }

    return 1;
}

static int parser_state_push_semantic(struct parser_state *state, struct semantic_item item) {
    if (!semantic_stack_push(&state->semantics, item)) {
        parser_state_free(state);
        parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
        return 0;
    }

    return 1;
}

static int parser_state_pop_expected_semantic(struct parser_state *state, struct semantic_item *item, char expected) {
    *item = semantic_stack_peek(&state->semantics);

    if (item->symbol != expected) {
        parser_state_report_error(state, L4DEN_CONSTRUCT_ERROR, expected, item->symbol);
        return 0;
    }

    semantic_stack_pop(&state->semantics);

    return 1;
}

static int parser_state_init(struct parser_state *state, const char *input, size_t inlen, struct l4den_error *error) {
    state->error = error;
    if (inlen > 0 && !input) {
        parser_state_free(state);
        parser_state_report_error(state, L4DEN_ERROR_NULL_BUFFER, 0, 0);
        return 0;
    }

    if (!symbol_stack_init(&state->symbols)) {
        parser_state_free(state);
        parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
        return 0;
    }

    if (!semantic_stack_init(&state->semantics)) {
        parser_state_free(state);
        parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
        return 0;
    }

    if (!parser_state_push_symbol(state, TERMINAL_EOF))
        return 0;

    if (!parser_state_push_symbol(state, NONTERMINAL_L4DEN))
        return 0;

    l4den_scanner_load(&state->scanner, input, inlen);

    return 1;
}

static int parser_state_step_expect_terminal(struct parser_state *state, char terminal) {
    struct semantic_item result;

    result.symbol = l4den_scanner_next(&state->scanner, &result.terminal.start, &result.terminal.len);

    if (result.symbol != terminal) {
        parser_state_report_error(state, L4DEN_PARSE_ERROR, terminal, result.symbol);
        return -1;
    }

    if (!parser_state_push_semantic(state, result))
        return -1;

    symbol_stack_pop(&state->symbols);

    return 0;
}

static int parser_state_step_expand_nonterminal(struct parser_state *state, char nonterminal) {
    /* TODO: Eventually should have l4den_scanner_peek to cache this scan */
    struct l4den_scanner save = state->scanner;
    struct semantic_item peek;

    peek.symbol = l4den_scanner_next(&save, &peek.terminal.start, &peek.terminal.len);

    /* remove the non-terminal to be replaced by its rhs */
    symbol_stack_pop(&state->symbols);

    /* LL(1) parse table */
    switch (nonterminal) {
        case NONTERMINAL_L4DEN:
            switch (peek.symbol) {
                case TERMINAL_EOF:   case TERMINAL_LITERAL:
                case TERMINAL_OPENB: case TERMINAL_BEGIN:
                    if (!parser_state_push_symbol(state, RULE_0))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_BODY))
                        return -1;
                    return 0;
            }
            break;

        case NONTERMINAL_BODY:
            switch (peek.symbol) {
                case TERMINAL_EOF:    case TERMINAL_CLOSEP:
                case TERMINAL_CLOSEB: case TERMINAL_END:
                    if (!parser_state_push_symbol(state, RULE_1))
                        return -1;
                    return 0;

                case TERMINAL_LITERAL:
                case TERMINAL_OPENB:
                case TERMINAL_BEGIN:
                    if (!parser_state_push_symbol(state, RULE_2))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_BODY))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_ENTITY))
                        return -1;
                    return 0;
            }
            break;

        case NONTERMINAL_ENTITY:
            switch (peek.symbol) {
                case TERMINAL_LITERAL:
                case TERMINAL_OPENB:
                case TERMINAL_BEGIN:
                    if (!parser_state_push_symbol(state, RULE_3))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_OPT_ASSIGNMENT))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_VALUE))
                        return -1;
                    return 0;
            }
            break;

        case NONTERMINAL_OPT_ASSIGNMENT:
            switch (peek.symbol) {
                case TERMINAL_EOF:    case TERMINAL_LITERAL:
                case TERMINAL_CLOSEP: case TERMINAL_OPENB:
                case TERMINAL_CLOSEB: case TERMINAL_BEGIN:
                case TERMINAL_END:
                    if (!parser_state_push_symbol(state, RULE_4))
                        return -1;
                    return 0;

                case TERMINAL_EQUALS:
                    if (!parser_state_push_symbol(state, RULE_5))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_VALUE))
                        return -1;
                    if (!parser_state_push_symbol(state, TERMINAL_EQUALS))
                        return -1;
                    return 0;
            }
            break;

        case NONTERMINAL_VALUE:
            switch (peek.symbol) {
                case TERMINAL_LITERAL:
                    if (!parser_state_push_symbol(state, RULE_6))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_SCALAR))
                        return -1;
                    return 0;

                case TERMINAL_OPENB:
                case TERMINAL_BEGIN:
                    if (!parser_state_push_symbol(state, RULE_7))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_OBJECT))
                        return -1;
                    return 0;
            }
            break;

        case NONTERMINAL_SCALAR:
            switch (peek.symbol) {
                case TERMINAL_LITERAL:
                    if (!parser_state_push_symbol(state, RULE_8))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_TYPE))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_ARGS))
                        return -1;
                    if (!parser_state_push_symbol(state, TERMINAL_LITERAL))
                        return -1;
                    return 0;
            }
            break;

        case NONTERMINAL_ARGS:
            switch (peek.symbol) {
                case TERMINAL_EOF:     case TERMINAL_EQUALS:
                case TERMINAL_LITERAL: case TERMINAL_CLOSEP:
                case TERMINAL_COLON:   case TERMINAL_OPENB:
                case TERMINAL_CLOSEB:  case TERMINAL_BEGIN:
                case TERMINAL_END:
                    if (!parser_state_push_symbol(state, RULE_9))
                        return -1;
                    return 0;

                case TERMINAL_OPENP:
                    if (!parser_state_push_symbol(state, RULE_10))
                        return -1;
                    if (!parser_state_push_symbol(state, TERMINAL_CLOSEP))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_BODY))
                        return -1;
                    if (!parser_state_push_symbol(state, TERMINAL_OPENP))
                        return -1;
                    return 0;
            }
            break;

        case NONTERMINAL_TYPE:
            switch (peek.symbol) {
                case TERMINAL_EOF:     case TERMINAL_EQUALS:
                case TERMINAL_LITERAL: case TERMINAL_CLOSEP:
                case TERMINAL_OPENB:   case TERMINAL_CLOSEB:
                case TERMINAL_BEGIN:   case TERMINAL_END:
                    if (!parser_state_push_symbol(state, RULE_11))
                        return -1;
                    return 0;

                case TERMINAL_COLON:
                    if (!parser_state_push_symbol(state, RULE_12))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_ARGS))
                        return -1;
                    if (!parser_state_push_symbol(state, TERMINAL_LITERAL))
                        return -1;
                    if (!parser_state_push_symbol(state, TERMINAL_COLON))
                        return -1;
                    return 0;
            }
            break;

        case NONTERMINAL_OBJECT:
            switch (peek.symbol) {
                case TERMINAL_OPENB:
                    if (!parser_state_push_symbol(state, RULE_13))
                        return -1;
                    if (!parser_state_push_symbol(state, TERMINAL_CLOSEB))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_BODY))
                        return -1;
                    if (!parser_state_push_symbol(state, TERMINAL_OPENB))
                        return -1;
                    return 0;

                case TERMINAL_BEGIN:
                    if (!parser_state_push_symbol(state, RULE_14))
                        return -1;
                    if (!parser_state_push_symbol(state, TERMINAL_END))
                        return -1;
                    if (!parser_state_push_symbol(state, NONTERMINAL_BODY))
                        return -1;
                    if (!parser_state_push_symbol(state, TERMINAL_BEGIN))
                        return -1;
                    return 0;
            }
            break;
    }

    /* If we got here, then we hit a blank in the table */
    parser_state_report_error(state, L4DEN_PARSE_ERROR, nonterminal, peek.symbol);
    return -1;
}

static int parser_state_body_to_accept(struct parser_state *state) {
    struct semantic_item top;
    struct l4den_entry empty = {0};

    if (!parser_state_pop_expected_semantic(state, &top, NONTERMINAL_BODY))
        return -1;

    if (!entry_stack_push(&top.body.entries, empty)) {
        parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
        return -1;
    }

    top.accept.entries = top.body.entries.contents;
    top.symbol = NONTERMINAL_L4DEN;

    if (!parser_state_push_semantic(state, top))
        return -1;

    return 1; /* Special case: return 1 instead of 0 since we are accepting */
}

static int parser_state_add_empty_body(struct parser_state *state) {
    struct semantic_item created;
    created.symbol = NONTERMINAL_BODY;
    if (!entry_stack_init(&created.body.entries)) {
        parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
        return -1;
    }

    if (!parser_state_push_semantic(state, created))
        return -1;

    return 0;
}

static int parser_state_append_to_body(struct parser_state *state) {
    struct semantic_item body, entity;

    if (!parser_state_pop_expected_semantic(state, &body, NONTERMINAL_BODY))
        return -1;

    if (!parser_state_pop_expected_semantic(state, &entity, NONTERMINAL_ENTITY))
        return -1;

    if (!entry_stack_push(&body.body.entries, entity.entity.entry)) {
        parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
        return -1;
    }

    if (!parser_state_push_semantic(state, body))
        return -1;

    return 0;
}

static int parser_state_construct_entity(struct parser_state *state) {
    struct semantic_item value, opt_assignment, entity;

    if (!parser_state_pop_expected_semantic(state, &opt_assignment, NONTERMINAL_OPT_ASSIGNMENT))
        return -1;

    if (!parser_state_pop_expected_semantic(state, &value, NONTERMINAL_VALUE))
        return -1;

    entity.symbol = NONTERMINAL_ENTITY;

    if (opt_assignment.opt_assignment.value) {
        entity.entity.entry.value = opt_assignment.opt_assignment.value;
        entity.entity.entry.key = malloc(sizeof *entity.entity.entry.key);
        if (entity.entity.entry.key == 0) {
            parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
            return -1;
        }
        *(entity.entity.entry.key) = value.value.value;
    } else {
        entity.entity.entry.key = 0;
        entity.entity.entry.value = malloc(sizeof *entity.entity.entry.value);
        if (entity.entity.entry.value == 0) {
            parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
            return -1;
        }
        *(entity.entity.entry.value) = value.value.value;
    }

    if (!parser_state_push_semantic(state, entity))
        return -1;

    return 0;
}

static int parser_state_add_empty_assignment(struct parser_state *state) {
    struct semantic_item created;
    created.symbol = NONTERMINAL_OPT_ASSIGNMENT;
    created.opt_assignment.value = 0;

    if (!parser_state_push_semantic(state, created))
        return -1;

    return 0;
}

static int parser_state_construct_assignment(struct parser_state *state) {
    struct semantic_item equals, value, result;

    if (!parser_state_pop_expected_semantic(state, &value, NONTERMINAL_VALUE))
        return -1;

    if (!parser_state_pop_expected_semantic(state, &equals, TERMINAL_EQUALS))
        return -1;

    result.symbol = NONTERMINAL_OPT_ASSIGNMENT;
    result.opt_assignment.value = malloc(sizeof *result.opt_assignment.value);
    if (result.opt_assignment.value == 0) {
        parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
        return -1;
    }
    *(result.opt_assignment.value) = value.value.value;


    if (!parser_state_push_semantic(state, result))
        return -1;

    return 0;
}

static int parser_state_scalar_to_value(struct parser_state *state) {
    struct semantic_item scalar;

    if (!parser_state_pop_expected_semantic(state, &scalar, NONTERMINAL_SCALAR))
        return -1;

    /* Converting scalar to value is a nop */
    scalar.symbol = NONTERMINAL_VALUE;
    if (!parser_state_push_semantic(state, scalar))
        return -1;

    return 0;
}

static int parser_state_object_to_value(struct parser_state *state) {
    struct semantic_item object;

    if (!parser_state_pop_expected_semantic(state, &object, NONTERMINAL_OBJECT))
        return -1;

    /* Converting object to value is a nop */
    object.symbol = NONTERMINAL_VALUE;
    if (!parser_state_push_semantic(state, object))
        return -1;

    return 0;
}

static int parser_state_construct_scalar(struct parser_state *state) {
    struct semantic_item literal, args, type, scalar;

    if (!parser_state_pop_expected_semantic(state, &type, NONTERMINAL_TYPE))
        return -1;

    if (!parser_state_pop_expected_semantic(state, &args, NONTERMINAL_ARGS))
        return -1;

    if (!parser_state_pop_expected_semantic(state, &literal, TERMINAL_LITERAL))
        return -1;

    scalar.symbol = NONTERMINAL_SCALAR;
    scalar.scalar.value.is_value = 1;
    scalar.scalar.value.value.type = type.type.type;
    ALLOCATE_OR(scalar.scalar.value.value.value) {
        parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
        return -1;
    }
    scalar.scalar.value.value.value->value = literal.terminal.start;
    scalar.scalar.value.value.value->length = literal.terminal.len;
    scalar.scalar.value.value.value->arguments = args.args.entries;

    if (!parser_state_push_semantic(state, scalar))
        return -1;

    return 0;
}

static int parser_state_add_empty_args(struct parser_state *state) {
    struct semantic_item created;
    created.symbol = NONTERMINAL_ARGS;

    created.args.entries = 0;

    if (!parser_state_push_semantic(state, created))
        return -1;

    return 0;
}

static int parser_state_construct_args(struct parser_state *state) {
    struct semantic_item open, body, close;
    struct l4den_entry empty = {0};

    if (!parser_state_pop_expected_semantic(state, &close, TERMINAL_CLOSEP))
        return -1;

    if (!parser_state_pop_expected_semantic(state, &body, NONTERMINAL_BODY))
        return -1;

    if (!parser_state_pop_expected_semantic(state, &open, TERMINAL_OPENP))
        return -1;

    if (!entry_stack_push(&body.body.entries, empty)) {
        parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
        return -1;
    }
    body.accept.entries = body.body.entries.contents;
    body.symbol = NONTERMINAL_ARGS;

    if (!parser_state_push_semantic(state, body))
        return -1;

    return 0;
}

static int parser_state_add_empty_type(struct parser_state *state) {
    struct semantic_item created;

    created.symbol = NONTERMINAL_TYPE;

    created.type.type = 0;

    if (!parser_state_push_semantic(state, created))
        return -1;

    return 0;
}

static int parser_state_construct_type(struct parser_state *state) {
    struct semantic_item colon, literal, args, type;

    if (!parser_state_pop_expected_semantic(state, &args, NONTERMINAL_ARGS))
        return -1;

    if (!parser_state_pop_expected_semantic(state, &literal, TERMINAL_LITERAL))
        return -1;

    if (!parser_state_pop_expected_semantic(state, &colon, TERMINAL_COLON))
        return -1;

    ALLOCATE_OR(type.type.type) {
        parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
        return -1;
    }

    type.type.type->value = literal.terminal.start;
    type.type.type->length = literal.terminal.len;
    type.type.type->arguments = args.args.entries;

    if (!parser_state_push_semantic(state, type))
        return -1;

    return 0;
}

static int parser_state_construct_list(struct parser_state *state) {
    struct semantic_item open, body, close, object;
    struct l4den_entry empty = {0};

    if (!parser_state_pop_expected_semantic(state, &close, TERMINAL_CLOSEB))
        return -1;

    if (!parser_state_pop_expected_semantic(state, &body, NONTERMINAL_BODY))
        return -1;

    if (!parser_state_pop_expected_semantic(state, &open, TERMINAL_OPENB))
        return -1;

    object.object.value.is_value = 0;
    object.object.value.object.ordered = 1;
    if (!entry_stack_push(&body.body.entries, empty)) {
        parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
        return -1;
    }
    object.object.value.object.body = body.body.entries.contents;
    object.symbol = NONTERMINAL_OBJECT;

    if (!parser_state_push_semantic(state, object))
        return -1;

    return 0;
}

static int parser_state_construct_set(struct parser_state *state) {
    struct semantic_item open, body, close, object;
    struct l4den_entry empty = {0};

    if (!parser_state_pop_expected_semantic(state, &close, TERMINAL_END))
        return -1;

    if (!parser_state_pop_expected_semantic(state, &body, NONTERMINAL_BODY))
        return -1;

    if (!parser_state_pop_expected_semantic(state, &open, TERMINAL_BEGIN))
        return -1;

    object.object.value.is_value = 0;
    object.object.value.object.ordered = 0;
    if (!entry_stack_push(&body.body.entries, empty)) {
        parser_state_report_error(state, L4DEN_ERROR_OUT_OF_MEMORY, 0, 0);
        return -1;
    }
    object.object.value.object.body = body.body.entries.contents;
    object.symbol = NONTERMINAL_OBJECT;

    if (!parser_state_push_semantic(state, object))
        return -1;

    return 0;
}

static int parser_state_step_evaluate_rule(struct parser_state *state, char rule) {
    /* Discard the rule we are about to process */
    symbol_stack_pop(&state->symbols);

    switch (rule) {
        case RULE_0: /* L4DEN -> BODY $end */
            return parser_state_body_to_accept(state);
        case RULE_1: /* BODY -> */
            return parser_state_add_empty_body(state);
        case RULE_2: /* BODY -> ENTITY BODY */
            return parser_state_append_to_body(state);
        case RULE_3: /* ENTITY -> VALUE OPT_ASSIGNMENT */
            return parser_state_construct_entity(state);
        case RULE_4: /* OPT_ASSIGNMENT -> */
            return parser_state_add_empty_assignment(state);
        case RULE_5: /* OPT_ASSIGNMENT -> = VALUE */
            return parser_state_construct_assignment(state);
        case RULE_6: /* VALUE -> SCALAR */
            return parser_state_scalar_to_value(state);
        case RULE_7: /* VALUE -> OBJECT */
            return parser_state_object_to_value(state);
        case RULE_8: /* SCALAR -> literal ARGS TYPE */
            return parser_state_construct_scalar(state);
        case RULE_9: /* ARGS -> */
            return parser_state_add_empty_args(state);
        case RULE_10: /* ARGS -> ( BODY ) */
            return parser_state_construct_args(state);
        case RULE_11: /* TYPE -> */
            return parser_state_add_empty_type(state);
        case RULE_12: /* TYPE -> : literal ARGS */
            return parser_state_construct_type(state);
        case RULE_13: /* OBJECT -> [ BODY ] */
            return parser_state_construct_list(state);
        case RULE_14: /* OBJECT -> { BODY } */
            return parser_state_construct_set(state);

        default:
            parser_state_report_error(state, L4DEN_PARSE_ERROR, 'R', rule);
            return -1;
    }
}

/* Returns 0 -> ok, -1 -> error, 1 -> done */
static int parser_state_step(struct parser_state *state) {
    char top = symbol_stack_peek(&state->symbols);
    switch (top) {
        case TERMINAL_LITERAL: case TERMINAL_EOF:
        case TERMINAL_OPENP:   case TERMINAL_CLOSEP:
        case TERMINAL_COLON:   case TERMINAL_EQUALS:
        case TERMINAL_OPENB:   case TERMINAL_CLOSEB:
        case TERMINAL_BEGIN:   case TERMINAL_END:
            return parser_state_step_expect_terminal(state, top);

        case NONTERMINAL_L4DEN:  case NONTERMINAL_BODY:
        case NONTERMINAL_ENTITY: case NONTERMINAL_OPT_ASSIGNMENT:
        case NONTERMINAL_VALUE:  case NONTERMINAL_SCALAR:
        case NONTERMINAL_ARGS:   case NONTERMINAL_TYPE:
        case NONTERMINAL_OBJECT:
            return parser_state_step_expand_nonterminal(state, top);

        case RULE_0:  case RULE_1:  case RULE_2:  case RULE_3:  case RULE_4:
        case RULE_5:  case RULE_6:  case RULE_7:  case RULE_8:  case RULE_9:
        case RULE_10: case RULE_11: case RULE_12: case RULE_13: case RULE_14:
            return parser_state_step_evaluate_rule(state, top);

        default:
            parser_state_report_error(state, L4DEN_PARSE_ERROR, '*', top);
            return -1;
    }
}

/* Returns the gathered result, or null on error */
static struct l4den_entry *parser_state_result(struct parser_state *state) {
    struct semantic_item top = semantic_stack_peek(&state->semantics);

    if (top.symbol != NONTERMINAL_L4DEN) {
        parser_state_report_error(state, L4DEN_PARSE_ERROR, NONTERMINAL_BODY, top.symbol);
        return 0;
    }

    semantic_stack_pop(&state->semantics);

    return top.accept.entries;
}

struct l4den_entry *l4den_decode(const char *input, size_t inlen, struct l4den_error *error) {
    int i;
    struct l4den_entry *result;
    struct parser_state state = {0};

    if (!parser_state_init(&state, input, inlen, error))
        return 0;

    while (!(i = parser_state_step(&state)));

    if (i < 0)
        return 0;

    result = parser_state_result(&state);

    parser_state_free(&state);

    return result;
}

struct printer_state {
    struct semantic_stack tbp; /* to be printed, needs to be a different type */
    struct l4den_error *error;
    char *buf;
    size_t buflen;
};

size_t l4den_encode(char *buf, size_t buflen, const struct l4den_entry *data, struct l4den_error *error) {



    // TODO
    return 0;
}

size_t l4den_encode_size(const struct l4den_entry *data) {
    // TODO
    return 0;
}
