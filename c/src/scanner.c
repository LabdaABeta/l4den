#include "scanner.h"

#include <ctype.h>

void l4den_scanner_load(struct l4den_scanner *scanner, const char *input, size_t inlen) {
    scanner->input = input;
    scanner->len = inlen;
    scanner->row = 1;
    scanner->col = 1;
}

static void l4den_scanner_advance(struct l4den_scanner *scanner) {
    if (*scanner->input == '\n') {
        scanner->row++;
        scanner->col = 1;
    } else {
        scanner->col++;
    }

    scanner->input++;
    scanner->len--;
}

static int iswordy(char x) {
    switch (x) {
        case '(': case ')':
        case ':': case '=':
        case '[': case ']':
        case '{': case '}':
        case 0: case ' ':
        case '\t': case '\n':
            return 0;

        default:
            return 1;
    }
}

static char scanner_load_quoted(struct l4den_scanner *scanner, char **start, size_t *len) {
    char first = *scanner->input;
    int size = 0;
    int current;

    while (*scanner->input == first && scanner->len) {
        l4den_scanner_advance(scanner);
        size++;
    }

    *start = scanner->input;
    *len = 0;

    current = 0;
    while (current < size && scanner->len) {
        if (*scanner->input == first)
            current++;
        else
            current = 0;

        l4den_scanner_advance(scanner);
        *len++;
    }

    if (current != size)
        return SCANNER_ERROR;

    return TERMINAL_LITERAL;
}

static char scanner_load_literal(struct l4den_scanner *scanner, char **start, size_t *len) {
    if (*scanner->input == '"' || *scanner->input == '\'' || *scanner->input == '`') {
        scanner_load_quoted(scanner, start, len);
    } else {
        *start = scanner->input;
        *len = 0;

        while (iswordy(*scanner->input) && scanner->len) {
            *len++;
            l4den_scanner_advance(scanner);
        }

        return TERMINAL_LITERAL;
    }
}

char l4den_scanner_next(struct l4den_scanner *scanner, char **start, size_t *len) {
    while (isspace(*scanner->input) && scanner->len) {
        l4den_scanner_advance(scanner);
    }

    if (!scanner->len) {
        return TERMINAL_EOF;
    }

    switch (*scanner->input) {
        case '(': case ')':
        case ':': case '=':
        case '[': case ']':
        case '{': case '}':
            l4den_scanner_advance(scanner); return *scanner->input;

        default:
            return scanner_load_literal(scanner, start, len);
    }
}
