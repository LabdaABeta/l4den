#ifndef BUFFER_ELEMENT
    #error "BUFFER_ELEMENT not defined, should be the type of elements in the buffer."
#endif

#ifndef PREFIX
    #error "PREFIX not defined, should be the prefix to add to functions and types."
#endif

#define PP_CONCAT_IMPL(X, Y) X ## _ ## Y
#define PP_CONCAT(X, Y) PP_CONCAT_IMPL(X, Y)
#define P(X) PP_CONCAT(PREFIX, X)

struct PREFIX {
    BUFFER_ELEMENT *buffer;
    size_t size;
};

int P(init)(struct PREFIX *buffer);



#undef PP_CONCAT_IMPL
#undef PP_CONCAT
#undef PREFIX
#undef BUFFER_ELEMENT
