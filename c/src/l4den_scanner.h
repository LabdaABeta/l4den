/*!
 * \file l4den_scanner.h
 * \author Louis Burke
 * \brief This header file defines an API for scanning L4DEN tokens.
 *
 * This is mostly used by the internals of the API defined in l4den.h.
 */

#ifndef L4DEN_SCANNER_H
#define L4DEN_SCANNER_H

#define TERMINAL_LITERAL '"'
#define TERMINAL_EOF     '$'
#define TERMINAL_OPENP   '('
#define TERMINAL_CLOSEP  ')'
#define TERMINAL_COLON   ':'
#define TERMINAL_EQUALS  '='
#define TERMINAL_OPENB   '['
#define TERMINAL_CLOSEB  ']'
#define TERMINAL_BEGIN   '{'
#define TERMINAL_END     '}'

#define NONTERMINAL_L4DEN          'L'
#define NONTERMINAL_BODY           'B'
#define NONTERMINAL_ENTITY         'E'
#define NONTERMINAL_OPT_ASSIGNMENT '?'
#define NONTERMINAL_KEY            'K'
#define NONTERMINAL_VALUE          'V'
#define NONTERMINAL_SCALAR         'S'
#define NONTERMINAL_ARGS           'A'
#define NONTERMINAL_TYPE           'T'
#define NONTERMINAL_OBJECT         'O'
#define NONTERMINAL_LIST           '*'
#define NONTERMINAL_SET            '@'

struct l4den_scanner {
    const char *input;
    int row;
    int col;
};

struct l4den_scannerx {
    char (*nextchr)(void*);
    void *arg;
    int row;
    int col;
};

struct l4den_scanner_file {
    FILE *input;
    int row;
    int col;
};

void l4den_scanner_load(struct l4den_scanner *scanner, const char *input);
void l4den_scannerx_load(struct l4den_scannerx *scanner, char (*nextchr)(void*), void *arg);
#ifndef _STDIO_H
void l4den_scanner_file_load(struct l4den_scanner_file *scanner, FILE *input);
#endif /* _STDIO_H */

char l4den_scanner_next(struct l4den_scanner *scanner, char **start, size_t len);

// TODO: when documenting the two below need their buffers freed
char l4den_scannerx_next(struct l4den_scannerx *scanner, char **start, size_t len);
char l4den_scanner_file_next(struct l4den_scanner_file *scanner, char **start, size_t len);

#endif /* L4DEN_SCANNER_H */
